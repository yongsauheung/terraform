provider "azurerm" {
}

resource "azurerm_resource_group" "cc" {
  name = "rgContainerConference"
  location = "AustraliaSoutheast"
}

resource "azurerm_servicebus_namespace" "cc" {
  depends_on          = ["azurerm_resource_group.cc"]
  name                = "sbnsContainerConference"
  location            = "${azurerm_resource_group.cc.location}"
  resource_group_name = "${azurerm_resource_group.cc.name}"
  sku                 = "standard"
}

resource "azurerm_servicebus_topic" "cc" {
  name                = "sbTopicContainerConference"
  resource_group_name = "${azurerm_resource_group.cc.name}"
  namespace_name      = "${azurerm_servicebus_namespace.cc.name}"
  enable_partitioning = false
}

resource "azurerm_servicebus_subscription" "cc" {
  name                = "sbSubscriptionContainerConference"
  resource_group_name = "${azurerm_resource_group.cc.name}"
  namespace_name      = "${azurerm_servicebus_namespace.cc.name}"
  topic_name          = "${azurerm_servicebus_topic.cc.name}"
  max_delivery_count  = 1
}

output "sb_connection_string" {
  value = "${azurerm_servicebus_namespace.cc.default_primary_connection_string}"
}
