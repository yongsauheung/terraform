# Getting Started Terraform

Step-by-step instructions to provision an Azure Resource Group and Azure Service Bus using Terraform.

## az login
Login Azure CLI using your account. Follow the instructions from the CLI.
``` powershell
> az login
```
``` JSON
[
  {
    "cloudName": "AzureCloud",
    "id": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "isDefault": false,
    "name": "Your Subscription Name",
    "state": "Enabled",
    "tenantId": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
    "user": {
      "name": "xxxxxxxx@google.com",
      "type": "user"
    }
  }
]
```

## az account set
Set current subscription (only needed if you have multiple subscriptions).
``` powershell
> az account set --subscription="xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
```
``` JSON
{
  "subscriptionId": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
  "tenantId": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
```

## az ad sp create-for-rbac
Creates a service principal with role-based AC.
``` powershell
> az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
```
``` JSON
{
  "appId": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
  "displayName": "azure-cli-2018-05-24-06-09-06",
  "name": "http://azure-cli-2018-05-24-06-09-06",
  "password": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
  "tenant": "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
```

## Set Azure Environment Variables
Set environment variables required for Terraform to provision infrastructure on behalf of the service principal.
ARM_SUBSCRIPTION_ID is equavalent to Azure Subscription ID.
ARM_CLIENT_ID is equavalent to Service Principal ID.
ARM_CLIENT_SECRET is equavalent to Service Principal password.
ARM_TENANT_ID is equavalent to Azure Tenant ID.
``` sh
set ARM_SUBSCRIPTION_ID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
set ARM_CLIENT_ID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
set ARM_CLIENT_SECRET=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx
set ARM_TENANT_ID=xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx

# Not needed for public, required for usgovernment, german, china
set ARM_ENVIRONMENT=public
```

## terraform init
Initializes the current directory with the providers mentioned in HCL scripts.
``` powershell
> terraform init -input=false
```
```
Initializing provider plugins...
- Checking for available provider plugins on https://releases.hashicorp.com...
- Downloading plugin for provider "azurerm" (1.5.0)...

The following providers do not have any version constraints in configuration,
so the latest version was installed.

To prevent automatic upgrades to new major versions that may contain breaking
changes, it is recommended to add version = "..." constraints to the
corresponding provider blocks in configuration, with the constraint strings
suggested below.

* provider.azurerm: version = "~> 1.5"

Terraform has been successfully initialized!

You may now begin working with Terraform. Try running "terraform plan" to see
any changes that are required for your infrastructure. All Terraform commands
should now work.

If you ever set or change modules or backend configuration for Terraform,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```

## terraform plan
Plan the changes required against the current state, saving the plan to be executed later as tfplan. Using options ```-input=false``` tells Terraform not to prompt for confirmation.
``` powershell
> terraform plan -out=tfplan -input=false
```

```
Refreshing Terraform state in-memory prior to plan...
The refreshed state will be used to calculate this plan, but will not be
persisted to local or remote state storage.


------------------------------------------------------------------------

An execution plan has been generated and is shown below.
Resource actions are indicated with the following symbols:
  + create

Terraform will perform the following actions:

  + azurerm_resource_group.cc
      id:                                      <computed>
      location:                                "australiasoutheast"
      name:                                    "rgContainerConference"
      tags.%:                                  <computed>

  + azurerm_servicebus_namespace.cc
      id:                                      <computed>
      default_primary_connection_string:       <computed>
      default_primary_key:                     <computed>
      default_secondary_connection_string:     <computed>
      default_secondary_key:                   <computed>
      location:                                "australiasoutheast"
      name:                                    "sbnsContainerConference"
      resource_group_name:                     "rgContainerConference"
      sku:                                     "standard"
      tags.%:                                  <computed>

  + azurerm_servicebus_subscription.cc
      id:                                      <computed>
      auto_delete_on_idle:                     <computed>
      default_message_ttl:                     <computed>
      lock_duration:                           <computed>
      max_delivery_count:                      "1"
      name:                                    "sbSubscriptionContainerConference"
      namespace_name:                          "sbnsContainerConference"
      resource_group_name:                     "rgContainerConference"
      topic_name:                              "sbTopicContainerConference"

  + azurerm_servicebus_topic.cc
      id:                                      <computed>
      auto_delete_on_idle:                     <computed>
      default_message_ttl:                     <computed>
      duplicate_detection_history_time_window: <computed>
      enable_partitioning:                     "false"
      max_size_in_megabytes:                   <computed>
      name:                                    "sbTopicContainerConference"
      namespace_name:                          "sbnsContainerConference"
      resource_group_name:                     "rgContainerConference"
      status:                                  "Active"


Plan: 4 to add, 0 to change, 0 to destroy.

------------------------------------------------------------------------

This plan was saved to: tfplan

To perform exactly these actions, run the following command to apply:
    terraform apply "tfplan"
```

## terraform apply
Apply/Execute previously planned changes with the name ```tfplan```.  Using options ```-input=false``` tells Terraform not to prompt for confirmation.
``` powershell
> terraform apply -input=false tfplan
```
```
azurerm_resource_group.cc: Creating...
  location: "" => "australiasoutheast"
  name:     "" => "rgContainerConference"
  tags.%:   "" => "<computed>"
azurerm_resource_group.cc: Creation complete after 0s (ID: /subscriptions/.../resourceGroups/rgContainerConference)
azurerm_servicebus_namespace.cc: Creating...
  default_primary_connection_string:   "" => "<computed>"
  default_primary_key:                 "" => "<computed>"
  default_secondary_connection_string: "" => "<computed>"
  default_secondary_key:               "" => "<computed>"
  location:                            "" => "australiasoutheast"
  name:                                "" => "sbnsContainerConference"
  resource_group_name:                 "" => "rgContainerConference"
  sku:                                 "" => "standard"
  tags.%:                              "" => "<computed>"
azurerm_servicebus_namespace.cc: Still creating... (10s elapsed)
azurerm_servicebus_namespace.cc: Still creating... (20s elapsed)
azurerm_servicebus_namespace.cc: Still creating... (30s elapsed)
azurerm_servicebus_namespace.cc: Still creating... (40s elapsed)
azurerm_servicebus_namespace.cc: Still creating... (50s elapsed)
azurerm_servicebus_namespace.cc: Still creating... (1m0s elapsed)
azurerm_servicebus_namespace.cc: Still creating... (1m10s elapsed)
azurerm_servicebus_namespace.cc: Creation complete after 1m20s (ID: /subscriptions/...Bus/namespaces/sbnsContainerConference)
azurerm_servicebus_topic.cc: Creating...
  auto_delete_on_idle:                     "" => "<computed>"
  default_message_ttl:                     "" => "<computed>"
  duplicate_detection_history_time_window: "" => "<computed>"
  enable_partitioning:                     "" => "false"
  max_size_in_megabytes:                   "" => "<computed>"
  name:                                    "" => "sbTopicContainerConference"
  namespace_name:                          "" => "sbnsContainerConference"
  resource_group_name:                     "" => "rgContainerConference"
  status:                                  "" => "Active"
azurerm_servicebus_topic.cc: Creation complete after 4s (ID: /subscriptions/...ence/topics/sbTopicContainerConference)
azurerm_servicebus_subscription.cc: Creating...
  auto_delete_on_idle: "" => "<computed>"
  default_message_ttl: "" => "<computed>"
  lock_duration:       "" => "<computed>"
  max_delivery_count:  "" => "1"
  name:                "" => "sbSubscriptionContainerConference"
  namespace_name:      "" => "sbnsContainerConference"
  resource_group_name: "" => "rgContainerConference"
  topic_name:          "" => "sbTopicContainerConference"
azurerm_servicebus_subscription.cc: Creation complete after 4s (ID: /subscriptions/...ions/sbSubscriptionContainerConference)

Apply complete! Resources: 4 added, 0 changed, 0 destroyed.

Outputs:

sb_connection_string = Endpoint=sb://sbnscontainerconference.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=xxxxxxxxxxx=
```

## terraform output
Get output using the Terraform CLI.
``` powershell
> terraform output sb_connection_string
```
```
Endpoint=sb://sbnscontainerconference.servicebus.windows.net/;SharedAccessKeyName=RootManageSharedAccessKey;SharedAccessKey=xxxxxxxxxxx=
```